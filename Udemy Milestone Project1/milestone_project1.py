import random


def display_board(board):
    print()
    print(board[7] + ' | ' + board[8] + ' | ' + board[9])
    print()
    print(board[4] + ' | ' + board[5] + ' | ' + board[6])
    print()
    print(board[1] + ' | ' + board[2] + ' | ' + board[3])
    print()


def player_input(player1, player2):
    player1_marker = 'Wrong'
    while player1_marker not in ['X', 'O']:
        print()
        player1_marker = input(f'{player1} would you like to be X or O: ')
        if player1_marker not in ['X', 'O']:
            print("Wrong Input! Please choose 'X' or 'O'")
    if player1_marker == 'X':
        player2_marker = 'O'
    else:
        player2_marker = 'X'

    print()
    print(f'{player1} marker is {player1_marker}')
    print(f'{player2} marker is {player2_marker}')
    print()

    return (player1_marker, player2_marker)


def place_marker(board, marker, position):
    board[position] = marker


def win_check(board, mark):
    if board[7] == board[8] == board[9] == mark:
        return True
    elif board[4] == board[5] == board[6] == mark:
        return True
    elif board[1] == board[2] == board[3] == mark:
        return True
    elif board[7] == board[4] == board[1] == mark:
        return True
    elif board[8] == board[5] == board[2] == mark:
        return True
    elif board[9] == board[6] == board[3] == mark:
        return True
    elif board[7] == board[5] == board[3] == mark:
        return True
    elif board[9] == board[5] == board[1] == mark:
        return True
    return False


def choose_first(name1, name2):
    toss = random.randint(1, 2)
    if toss == 1:
        print(f"{name1} will go first")
        return (name1, name2)
    else:
        print(f"{name2} will go first")
        return (name2, name1)


def space_check(board, position):
    return board[position] == '-'


def full_board_check(board):
    return '-' not in board[1:]


def player_choice(board):
    choice = 'Wrong'
    isFree = False
    accepted_values = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    while choice not in accepted_values or isFree == False:
        choice = input("Enter next position (1,9): ")
        if choice not in accepted_values:
            print("Wrong Input! Please enter value in the range(1-9)")
        else:
            if space_check(board, int(choice)):
                isFree = True
            else:
                print("Space not free at entered position. Please enter again.")
    return int(choice)


def replay():
    choice = "Wrong"
    accepted_values = ['Y', 'N']
    while choice not in accepted_values:
        choice = input("Do you want to play again (Y or N):")
        if choice not in accepted_values:
            print("Wrong Input! Please enter Y or N")
    return True if choice == 'Y' else False


print('Welcome to Tic Tac Toe!')

game_on = True
while game_on:
    name1 = input("Enter Player1 name: ")
    name2 = input("Enter Player2 name: ")
    mark1, mark2 = player_input(name1, name2)
    player1, player2 = choose_first(name1, name2)
    if player1 == name1:
        player1_marker, player2_marker = mark1, mark2
    else:
        player1_marker, player2_marker = mark2, mark1

    board = ['-' for _ in range(10)]
    board[0] = ' '
    while True:
        display_board(board)
        print()
        print(f"{player1}'s turn")
        player1_choice = player_choice(board)
        place_marker(board, player1_marker, player1_choice)
        if win_check(board, player1_marker):
            display_board(board)
            print(f"!!! {player1} won !!!")
            break
        if full_board_check(board):
            display_board(board)
            print("Match ended in a draw")
            break

        display_board(board)
        print()
        print(f"{player2}'s turn")
        player2_choice = player_choice(board)
        place_marker(board, player2_marker, player2_choice)
        if win_check(board, player2_marker):
            display_board(board)
            print(f"!!! {player2} won !!!")
            break
        if full_board_check(board):
            display_board(board)
            print("Match ended in a draw")
            break

    game_on = replay()

print("Goodbye")
