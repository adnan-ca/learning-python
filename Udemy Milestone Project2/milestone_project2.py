import random

suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven',
         'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
values = {'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5, 'Six': 6, 'Seven': 7, 'Eight': 8, 'Nine': 9, 'Ten': 10, 'Jack': 10,
          'Queen': 10, 'King': 10, 'Ace': 11}

playing = True


class Card:

    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return f'{self.rank} of {self.suit}'


class Deck:

    def __init__(self):
        self.deck = []  # start with an empty list
        for suit in suits:
            for rank in ranks:
                self.deck.append(Card(suit, rank))

    def __str__(self):
        for card in self.deck:
            print(card)
        return ' '

    def shuffle(self):
        random.shuffle(self.deck)

    def deal(self):
        return self.deck.pop()


class Hand:
    def __init__(self):
        self.cards = []  # start with an empty list as we did in the Deck class
        self.value = 0   # start with zero value
        self.aces = 0    # add an attribute to keep track of aces

    def add_card(self, card):
        self.cards.append(card)
        self.value += values[card.rank]
        if card.rank == 'Ace':
            self.aces += 1

    def adjust_for_ace(self):
        while self.value > 21 and self.aces > 0:
            self.value -= 10
            self.aces -= 1


class Chips:

    def __init__(self):
        self.total = 100  # This can be set to a default value or supplied by a user input
        self.bet = 0

    def win_bet(self):
        self.total += self.bet

    def lose_bet(self):
        self.total -= self.bet


def take_bet(chips):

    try:
        bet = int(input("\nEnter your bet amount as integer: "))
        if bet > chips.total:
            print(
                f"\tSorry you can't place bet greater than {chips.total}.Please enter again.\n")
            return take_bet(chips)
    except ValueError:
        print("\tSorry entered value is not an integer! Please enter an integer.\n")
        return take_bet(chips)
    chips.bet = bet


def hit(deck, hand):
    hand.add_card(deck.deal())
    hand.adjust_for_ace()


def hit_or_stand(deck, hand):
    global playing  # to control an upcoming while loop

    while True:
        choice = input(
            "\nDo you want to hit(h) or stand(s)? Enter 'h' or 's': ").lower()
        if choice not in ['h', 's']:
            print("\nSorry! Please choose 'h' or 's'.\n")
        else:
            break

    if choice == 'h':
        hit(deck, hand)
    else:
        print("\nPlayer stands. Dealer is playing.\n")
        playing = False


def show_some(player, dealer):
    print("\nDealer's Card: ")
    print(" <card hidden>")
    print("", dealer.cards[1])
    print("\nPlayer's Card:", *player.cards, sep="\n ")


def show_all(player, dealer):
    print("\nDealer's Card: ", *dealer.cards, sep="\n ")
    print(f"Dealer's Value: {dealer.value}")
    print("\nPlayer's Card:", *player.cards, sep="\n ")
    print(f"Player's Value: {player.value}")


def player_busts(chips):
    print("\nPlayer busts!")
    chips.lose_bet()


def player_wins(chips):
    print("\nPlayer wins!")
    chips.win_bet()


def dealer_busts(chips):
    print("\nDealer busts!")
    chips.win_bet()


def dealer_wins(chips):
    print("\nDealer Wins!")
    chips.lose_bet()


def push():
    print("\nDealer and Player Tie. It's a push.")


while True:
    # Print an opening statement
    print('\nWelcome to the game of BlackJack! Get as close to 21 as you can without going over!\n\
    Dealer hits until she reaches 17. Aces count as 1 or 11.')

    # Create & shuffle the deck, deal two cards to each player
    deck = Deck()
    deck.shuffle()

    player_hand = Hand()
    player_hand.add_card(deck.deal())
    player_hand.add_card(deck.deal())

    dealer_hand = Hand()
    dealer_hand.add_card(deck.deal())
    dealer_hand.add_card(deck.deal())

    # Set up the Player's chips
    player_chips = Chips()

    # Prompt the Player for their bet
    take_bet(player_chips)

    # Show cards (but keep one dealer card hidden)
    show_some(player_hand, dealer_hand)

    while playing:  # recall this variable from our hit_or_stand function

        # Prompt for Player to Hit or Stand
        hit_or_stand(deck, player_hand)

        # Show cards (but keep one dealer card hidden)
        show_some(player_hand, dealer_hand)

        # If player's hand exceeds 21, run player_busts() and break out of loop
        if player_hand.value > 21:
            player_busts(player_chips)
            break

    # If Player hasn't busted, play Dealer's hand until Dealer reaches 17
    if player_hand.value <= 21:
        while dealer_hand.value < 17:
            hit(deck, dealer_hand)

        # Show all cards
        show_all(player_hand, dealer_hand)

        # Run different winning scenarios
        if dealer_hand.value > 21:
            dealer_busts(player_chips)
        elif dealer_hand.value > player_hand.value:
            dealer_wins(player_chips)
        elif player_hand.value > dealer_hand.value:
            player_wins(player_chips)
        else:
            push()

    # Inform Player of their chips total
    print(f"\nPlayer winnings total stands at {player_chips.total}.")

    # Ask to play again
    while True:
        game_on = input(
            "\nWould you like to play again? Enter 'Y' or 'N'.").lower()
        if game_on not in ['y', 'n']:
            print("\tSorry, invalid input! Please enter 'y' or 'n'.")
        else:
            break
    if game_on == 'n':
        print("\nAdios")
        break
